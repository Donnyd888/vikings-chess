jest.mock('socket.io-client');
const io = require('socket.io-client');

describe('Testing Socket IO connection making sure it works', () => {
    let socket;

    beforeAll(async () => {
        io.connect.mockReturnValue({
            connected: true,
            on: jest.fn((event, callback) => {
                if (event === 'connect') {
                    callback(); 
                }
            }),
            disconnect: jest.fn()
        });
        socket = io.connect();
        await new Promise(resolve => setTimeout(resolve, 100)); 
    });

    test('Socket.IO connection', () => {
        expect(socket.connected).toBeTruthy(); 
     
    });
});
