

const { JSDOM } = require('jsdom');

global.fetch = jest.fn(() =>
    Promise.resolve({
        ok: true,
        json: () => Promise.resolve({ message: 'User successfully registered' }),
    })
);

async function registerUser(event) {
    event.preventDefault(); // Prevent the default form submission behavior

    const email = document.getElementById('email').value;
    const user_name = document.getElementById("userName").value;
    const password = document.getElementById('pwd').value;
    const confirm_password = document.getElementById('reenter_password').value;

    const data = {
        "email": email,
        "user_name": user_name,
        "password": password,
        "confirm_password": confirm_password
    };

    try {
        const response = await fetch('v1/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        let responseMessage;

        if (response.ok) {
            responseMessage = 'Registration successful:';
        } else {
            responseMessage = 'Registration failed:';
        }

        try {
            const responseData = await response.json();
            if (responseData && responseData.message) {
                responseMessage += ' ' + responseData.message;
            }
        } catch (error) {}

        console.log(responseMessage);
        document.getElementById('registerAnnounce').textContent = responseMessage;

    } catch (error) {
        console.error('Error occurred during registration:', error);
        document.getElementById('registerAnnounce').textContent = 'Error occurred during registration: ' + error.message;
    }
}

describe('registerUser function', () => {
    let dom;

    beforeEach(() => {
        dom = new JSDOM(`
            <html>
            <body>
                <form id="registerForm">
                    <input type="email" id="email" value="test@example.com">
                    <input type="text" id="userName" value="testuser">
                    <input type="password" id="pwd" value="password">
                    <input type="password" id="reenter_password" value="password">
                    <button id="submitButton">Submit</button>
                </form>
                <div id="registerAnnounce"></div>
            </body>
            </html>
        `);
        global.document = dom.window.document;
        global.window = dom.window;
    });

    afterEach(() => {
        delete global.document;
        delete global.window;
    });

    test('should send a POST request to the correct endpoint', async () => {
        const event = { preventDefault: jest.fn() };
        await registerUser(event);

        expect(fetch).toHaveBeenCalledWith('v1/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: 'test@example.com',
                user_name: 'testuser',
                password: 'password',
                confirm_password: 'password'
            })
        });
    });

    
    test('should display success message if registration is successful', async () => {
        const event = { preventDefault: jest.fn() };
        await registerUser(event);

        expect(document.getElementById('registerAnnounce').textContent).toContain('Registration successful:');
    });
});